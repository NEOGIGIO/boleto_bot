# https://api.telegram.org/bot1121744537:AAGEeVy_vZGk4c33siUxsGbIKsXDWOhjGKc/setWebhook?url=https%3A%2F%2Fus-central1-axial-radius-272516.cloudfunctions.net%2Fbrinvoicebot

# main.py
import telegram
import logging
import requests
import json
# import pdf2image
import numpy
import zbar
import cv2


TELEGRAM_TOKEN = "1121744537:AAGEeVy_vZGk4c33siUxsGbIKsXDWOhjGKc"


def barcode2linha_digitavel(barcode):
    if len(barcode) != 44: 
        logging.warning('Barcode not completely read')
    field_1 = barcode[0:4] + barcode[19:24]
    field_2 = barcode[24:34]
    field_3 = barcode[34:]
    field_4 = barcode[4]
    field_5 = barcode[5:19]

    return f'{field_1}{verifier(field_1)}{field_2}{verifier(field_2)}{field_3}{verifier(field_3)}{field_4}{field_5}'


def verifier(field):
    total = 0
    for index, number in enumerate(reversed(field)):
        if (index % 2) == 0:
            if int(number) * 2 >= 10:
                current = 1 + (int(number) * 2) % 10
            else:
                current = int(number) * 2
        else:
            current = int(number)
        total += current
    return (10 - (total % 10)) % 10


def scan_linha_digitavel(path):
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    scanner = zbar.Scanner()
    detected_barcodes = scanner.scan(image)

    for barcode in detected_barcodes:
        return barcode2linha_digitavel(barcode.data.decode("UTF-8"))


def download_file(file_id):
    file = json.loads(requests.get(f'https://api.telegram.org/bot{TELEGRAM_TOKEN}/getFile?file_id={file_id}').content)
    file_path = file['result']['file_path']
    image = requests.get(f'https://api.telegram.org/file/bot{TELEGRAM_TOKEN}/{file_path}').content
    file_name = file_path.split('/')[-1]
    open('/tmp/' + file_name, 'wb').write(image)
    return '/tmp/' + file_name


def br_invoice_bot(request):
    bot = telegram.Bot(TELEGRAM_TOKEN)

    if request.method == 'POST':
        update = telegram.Update.de_json(request.get_json(force=True), bot)
        chat_id = update.message.chat.id
        try:
            file_id = update.message.photo[-1].file_id
            try:
                file = download_file(file_id)
                linha_digitavel = scan_linha_digitavel(file)
                if len(linha_digitavel) == 47:
                    bot.send_message(chat_id=chat_id, text=linha_digitavel)
                else:
                    bot.send_message(chat_id=chat_id, text='Desculpe, não consegui encontrar o codigo de barras')
            except: 
                bot.send_message(chat_id=chat_id, text='Erro ao ler imagem')
        except:
            bot.send_message(chat_id=chat_id, text="Envie uma foto")
            return 'did not receive a photo'
        
    return 'ok'